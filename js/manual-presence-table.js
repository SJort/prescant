//creates the manual presences table. separate method because the checkboxes are dynamic
function loadManualPresenceTable(table, url, request, headers) {
    log("Loading manual presence table...");
    emptyPresenceTable();
    let finalURL = url + request;
    log("Filling table with URL: '" + finalURL + "'...");
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: finalURL,
        success: function (rowItems) {
            //we are retrieving an array of arrays; the rows with for each row the column items
            $.each(rowItems, function (rIndex, rItem) {

                //retrieve the just inserted row, variable is initialized in scope because checkbox listener needs a unique variable
                let currentRow = table.insertRow();

                //create empty cells in the just inserted row
                for (let column = 0; column < headers.length; column++) {
                    currentRow.insertCell();
                }

                log("Inserting student: name=" + rItem.name + ", studentId=" + rItem.personID + ", present=" + rItem.isPresent);

                //inject student name
                currentRow.cells[0].innerText = rItem.name;

                currentRow.cells[1].innerText = rItem.infix;
                currentRow.cells[2].innerText = rItem.lastname;
                currentRow.cells[3].innerText = rItem.personID;

                //presence checkbox
                let checkbox = document.createElement('input');
                checkbox.setAttribute("type", "checkbox");

                //modified indicator, always there because otherwise when inserted on interaction, the table shifts
                let modifiedCell = currentRow.cells[5];
                modifiedCell.innerText = "modified";
                modifiedCell.setAttribute("style", "opacity: 0");

                //save initial value so we can determine if the value is altered
                checkbox.setAttribute("originalValue", rItem.isPresent);

                //set checkbox checked on basis of the saved initial value
                checkbox.checked = checkbox.getAttribute("originalValue") === "1";

                //custom jQuery listener method, should work on custom classes, but we are using default checkbox:
                // $(checkbox).on("change", function () {

                checkbox.addEventListener( 'change', function() {
                    log("Checkbox at row with studentnumber " + rItem.personID + " changed to " + this.checked);

                    updatePresence(rItem.personID, checkbox.checked ? 1 : 0);

                    //handle the modified indicator:

                    //if the original value matches the current value, hide the modified indicator
                    if (checkbox.getAttribute("originalValue") === "1" && this.checked || checkbox.getAttribute("originalValue") === "0" && !this.checked) {
                        //toggle visibility because its always there
                        modifiedCell.setAttribute("style", "opacity: 0");
                    }
                    //else show the modified indicator
                    else {
                        modifiedCell.setAttribute("style", "opacity: 1");
                        //bootstrap theme color
                        modifiedCell.className = "text-warning";
                    }
                });

                //add the created checkbox to the row
                currentRow.cells[4].appendChild(checkbox);
            });

            fillHeader(table, headers);
        },
        error: function () {
            errorLog("Can't load json.");
        }
    });
}

function updatePresence(studentId, present){
    let query = parseSelectorQuery();
    query = query + "&phoneID=1&studentID=" + studentId;
    if(present){
        query = query + "&request=setPresence";
    }
    else{
        query = query + "&request=deletePresence";
    }
    log("Using query '" + query + "' to update presence of " + studentId);

    $.ajax({
        type: 'GET',
        url: query,
        success: function () {
            log('Presence of ' + studentId + ' updated to ' + present);
        },
        error: function () {
            errorLog("Update presence query failed");
        }
    });
}
