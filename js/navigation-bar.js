$(document).ready(function () {
    console.log("Loading navigation bar");
    let navBar = document.getElementById("navigationBar");
    $("#navigationBar").attr("class", "navbar navbar-expand-md navbar-light bg-light");
    $("#navigationBar").load("/html/navigation-bar.php", function () {
        //this is a callback function, because load is asynchronous and setUser needs the loaded elements
        setUser();

        //dirty hack to remove the logout button when at the login page by detecting the login form
        if (null != document.getElementById("loginForm")) {
            hideSessionBar();
        }

        //highlight the current page in the navbar, wont work outside this scope
        $('li.active').removeClass('active');
        $('a[href="' + location.pathname + '"]').closest('li').addClass('active');
    });
});

function onLogoutButton() {
    log("Logging out!");

    $.ajax({
        type: 'GET',
        url: '/php/login-server.php?request=logout',
        success: function () {
            log("Logged out!");
            window.location.href = "../html/login.php";
        },
        error: function () {
            errorLog("Could not execute logout query.");
        }
    });
}

function setUser() {
    let userText = document.getElementById("loggedInAs");
    if (userText == null) {
        errorLog("UserText was null.");
        return;
    }
    // userText.innerText = "Logged in as: " + "Jort";
}

function hideSessionBar() {
    log("Hid session bar");
    let sessionBar = document.getElementById("sessionBar");
    sessionBar.setAttribute("style", "visibility:hidden");
}
