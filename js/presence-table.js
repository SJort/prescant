//empties the presence table
function emptyPresenceTable() {
    log("Emptying presence table...")
    const table = document.getElementById("presenceTable");
    table.innerHTML = "";
}

/**
 * Fills the given table with json parsed from the given url, and fills the table header with the given array.
 * @param table The table to fill.
 * @param url The url to parse the json from.
 * @param headers An array of strings to fill the header with in the right order.
 */
function fillTable(table, url, request, headers) {
    emptyPresenceTable();
    let finalURL = url + request;
    log("Filling table with URL: '" + finalURL + "'...");
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: finalURL,
        success: function (json) {
            try{
                //fill values first, because insertRow then appends the new rows to the tbody
                $.each(json, function(key, value){
                    //key is a number here
                    let row = table.insertRow();
                    $.each(value, function(xKey, xValue){
                        //xKey is the actual key of that element here
                        let cell = row.insertCell();
                        cell.innerText = xValue;
                    });
                });

                fillHeader(table, headers);
            }
            catch(exception){
                errorLog("Can't fill table: " + exception);
            }
        },
        error: function () {
            errorLog("Can't load json.");
        }
    });
}

/**
 * Fills the given table with the given headers.
 * @param table The table to fill.
 * @param headers An array of strings to put in the header in the right order.
 */
function fillHeader(table, headers){
    //create and fill the table header
    let nHeader = table.createTHead();
    let nRow = nHeader.insertRow();
    $.each(headers, function (key, value) {
        let th = document.createElement('th');
        th.innerHTML = value;
        nRow.appendChild(th);
    });
}
