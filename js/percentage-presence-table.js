
async function handlePercentageTableWithSubX(table, query, request, headers) {
    let amountOfLessonQuery = query + "&request=getAmountLessonFromSubX";
    log("Using '" + amountOfLessonQuery + "' to get total amount of lessons...");
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: amountOfLessonQuery,
        success: function (json) {
            log("returned: " + JSON.stringify(json));
            try{
                $.each(json, function(key, value){
                    $.each(value, function(xKey, amountOfLessons){
                        //test if string only contains numbers, quick fix to sudden addition in json result
                        if(/^\d+$/.test(amountOfLessons)){
                            fillPercentageTable(table, query, request, amountOfLessons, headers);
                        }
                    });
                });
            }
            catch(exception){
                errorLog("Can't get total amount of lessons: " + exception);
            }
        },
        error: function () {
            errorLog("Can't load json.");
        }
    });
}

async function handlePercentageTable(table, query, request, headers) {
    let amountOfLessonQuery = query + "&request=getTotalAmountOfAllLessons";
    log("Using '" + amountOfLessonQuery + "' to get total amount of lessons...");
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: amountOfLessonQuery,
        success: function (json) {
            log("returned: " + JSON.stringify(json));
            try{
                $.each(json, function(key, value){
                   $.each(value, function(xKey, amountOfLessons){
                       //test if string only contains numbers, quick fix to sudden addition in json result
                       if(/^\d+$/.test(amountOfLessons)){
                           fillPercentageTable(table, query, request, amountOfLessons, headers);
                       }
                   });
                });
            }
            catch(exception){
                errorLog("Can't get total amount of lessons: " + exception);
            }
        },
        error: function () {
            errorLog("Can't load json.");
        }
    });
}

function fillPercentageTable(table, url, request, amountOfLessons, headers) {
    emptyPresenceTable();
    let finalURL = url + request;
    log("Filling table with URL: '" + finalURL + "'...");
    log("Total amount of lessons: " + amountOfLessons);

    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: finalURL,
        success: function (json) {
            try{
                //fill values first, because insertRow then appends the new rows to the tbody
                $.each(json, function(key, value){
                    //key is a number here
                    let row = table.insertRow();
                    $.each(value, function(xKey, xValue){
                        //xKey is the actual key of that element here
                        let cell = row.insertCell();
                        if(Number.isInteger(xValue)){
                            let outerBar = document.createElement("div");
                            outerBar.setAttribute("class", "progress");

                            let innerBar = document.createElement("div");
                            innerBar.setAttribute("role", "progressbar");
                            let percentage = (xValue/amountOfLessons)*100;
                            innerBar.setAttribute("class", "progress-bar bg-warning");
                            innerBar.setAttribute("style", "width: " + percentage + "%; color: black");
                            innerBar.innerHTML = xValue + " / " + amountOfLessons;

                            outerBar.appendChild(innerBar);
                            cell.appendChild(outerBar);
                        }
                        else{
                            cell.innerText = xValue;
                        }
                    });
                });

                fillHeader(table, headers);
            }
            catch(exception){
                errorLog("Can't fill table: " + exception);
            }
        },
        error: function () {
            errorLog("Can't load json.");
        }
    });
}
