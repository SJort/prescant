function fillStudentTable(table, query, request, headers) {
    log("Filling student table...");
    emptyPresenceTable();
    let finalURL = query + request;
    log("Filling table with URL: '" + finalURL + "'...");
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: finalURL,
        success: function (json) {
            try{
                //fill values first, because insertRow then appends the new rows to the tbody
                $.each(json, function(key, value){
                    //key is a number here
                    let row = table.insertRow();
                    let studentNumber = 666;
                    $.each(value, function(xKey, xValue){
                        //xKey is the actual key of that element here
                        let cell = row.insertCell();

                        //test if string only contains numbers
                        if(/^\d+$/.test(xValue)){
                            studentNumber = xValue;
                        }

                        cell.innerText = xValue;

                    });
                    let cell = row.insertCell();
                    let button = document.createElement("button");
                    button.setAttribute("class", "btn btn-warning");
                    button.innerText = "Check";
                    button.addEventListener("click", function(){
                          onStudentClicked(table, query, studentNumber);
                    });

                    cell.appendChild(button);
                });

                fillHeader(table, headers);
            }
            catch(exception){
                errorLog("Can't fill table: " + exception);
            }
        },
        error: function () {
            errorLog("Can't load json.");
        }
    });
}

function onStudentClicked (table, query, studentNumber) {
    log(studentNumber + " clicked!");
    let headers = ["Subject", "Name", "Infix", "Last name", "Presence"];
    let request = "&studentID=" + studentNumber + "&request=getAllSubFromStudX";
    emptyPresenceTable();
    log("Filling student subject presence table...");
    handlePercentageTable(table, query, request, headers);
}
