//this shit is just default for jquery. run all code from here
$(document).ready(function () {
    //previously loaded javascript files are accessible
    onLoad();
});

function onLoad() {
    log('Loading...');
    loadSelectors();
}

//loads the table with the presence of selected details
async function loadTable() {
    log("");
    let table = document.getElementById("presenceTable"); //jquery way doesn't work, function like insertRow not recognized
    emptyPresenceTable();
    let query = parseSelectorQuery();
    log("Parsed selectors.");
    await sleep(200);
    log("Now sending query...");

    if (sSubject !== defaultSelectorValue && sClass !== defaultSelectorValue && sWeek !== defaultSelectorValue && sLesson !== defaultSelectorValue) {
        //everything is defined
        //empty last header for modified indicator
        loadManualPresenceTable(table, query, "&request=getClassAttendanceFromCurr", ["Name", "Infix", "Last name", "Studentnumber", "Present", ""]);
        //fillTable(table, '/servercode.php?request=getClassAttendanceFromCurr', ["Name", "Infix", "Last name", "Studentnumber", "Present"]);
    }
    else if (sSubject !== defaultSelectorValue && sClass !== defaultSelectorValue && sWeek === defaultSelectorValue && sLesson === defaultSelectorValue) {
        //subject and class are defined
        handlePercentageTable(table, query, "&request=getClassAttendanceFromAllLes", ["Name", "Infix", "Last name", "Presence"]);
    }
    else if (sSubject !== defaultSelectorValue && sClass === defaultSelectorValue && sWeek === defaultSelectorValue && sLesson === defaultSelectorValue) {
        //subject is defined
        handlePercentageTableWithSubX(table, query,"&request=getAllClassAttendanceFromAllLes", ["Name", "Infix", "Last name", "Presence"]);
    }
    else if (sSubject === defaultSelectorValue && sClass !== defaultSelectorValue && sWeek === defaultSelectorValue && sLesson === defaultSelectorValue) {
        //class is defined
        fillStudentTable(table, query,"&request=getNamesFromClassX", ["Studentnumber", "First name", "Infix", "Last name", "Presence"]);
    }
    else if (sSubject === defaultSelectorValue && sClass === defaultSelectorValue && sWeek === defaultSelectorValue && sLesson === defaultSelectorValue) {
        //nothing is defined
        handlePercentageTable(table, query, "&request=getAllClassAttendanceFromAllSub", ["Name", "Infix", "Last name", "Presence"]);
    }
    else {
        table.innerHTML = "<div class='d-flex flex-sm-row flex-column justify-content-center text-danger'>This feature has not been implemented (yet).</div>"
        errorLog("Feature not implemented.");
    }
}


