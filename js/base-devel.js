//log in console and on website
function log(message) {
    let logBar = document.getElementById("logBar");
    console.log(message);

    if(logBar == null){
        return;
    }
    logBar.innerText = message + "\n" + logBar.innerText;
}

//same as log but for errors
function errorLog(message) {
    log("Error: " + message);
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
