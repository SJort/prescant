let amountOfWeeks = 9;
let amountOfLessons = 4;
let phpName = "servercode.php";

//current selected variables, null if user did not set or default value
let defaultSelectorValue = "All";
let sSubject = defaultSelectorValue;
let sClass = defaultSelectorValue;
let sWeek = defaultSelectorValue;
let sLesson = defaultSelectorValue;

function getInnerText(id){
    let text = document.getElementById(id);
    text = text.getAttribute("selectedValue");
    if(text === null){
        text = defaultSelectorValue;
    }
    return text;
}

function parseSelectorQuery(){
    log("Parsing selectors...");
    sSubject = getInnerText("subjectSelector");
    sClass = getInnerText("classSelector");
    sWeek = getInnerText("weekSelector");
    sLesson = getInnerText("lessonSelector");
    log("Selected: subject=" + sSubject + ", class=" + sClass + ", week=" + sWeek + ", lesson=" + sLesson);
    let query = "/servercode.php?request=set&subject=" + sSubject + "&class=" + sClass + "&week=" + sWeek + "&lesson=" + sLesson;
    log("Using query '" + query + "' to set variables");
    return query;
}

function loadSelectors() {
    log("Loading selectors...");

    if (!window.jQuery) {
        log("jQuery failed to load")
    }

    loadProperty("subjectSelector", "getAllSubjects", "subject");
    loadProperty("classSelector", "getAllClasses", "class");

    //hardcoded for now
    let selector = document.getElementById("weekSelector");
    addSelectorElement(selector, "weekSelector", defaultSelectorValue);
    for(let i = 1; i < amountOfWeeks + 1; i++){
        addSelectorElement(selector, "weekSelector", i);
    }

    selector = document.getElementById("lessonSelector");
    addSelectorElement(selector, "lessonSelector", defaultSelectorValue);
    for(let i = 1; i < amountOfLessons + 1; i++){
        addSelectorElement(selector, "lessonSelector", i);
    }
}

/**
 * Adds a selector element to a selector.
 * @param selector The selector to add the element to.
 * @param selectorId The ID of the selector.
 * @param data The data to add.
 */
function addSelectorElement(selector, selectorId, data){
    let selectorItem = document.createElement("A");
    let selectorText = document.createTextNode(data);
    selectorItem.appendChild(selectorText);
    selectorItem.className = "dropdown-item";
    selectorItem.setAttribute("href", "#");
    selectorItem.addEventListener("click", function(){
        //function needs a unique variable
        let uData = data;

        log(uData + " clicked!");
        selector.setAttribute("selectedValue", uData);
        //the button has the same id with "Button" attached to it
        let button = document.getElementById(selectorId + "Button");
        button.innerText = uData;
        loadTable();

    });
    selector.appendChild(selectorItem);
}


/**
 * Fills a selector with data.
 * @param elementId The ID of the element to fill.
 * @param request The get request used to retrieve the data to fill with. For example: getAllSubjects.
 * @param elementItemName How the data is called within json. For example for {"subject":"geschiedenis"} it should be subject.
 */
function loadProperty(elementId, request, elementItemName){
    log("Loading selector: elementId=" + elementId + ", request=" + request + ", elementItemName=" + elementItemName);
    let selector = document.getElementById(elementId);
    let requestQuery = phpName + "?request=" + request;
    log("Using request: " + requestQuery);
    $.ajax({
        type: "GET",
        dataType: "json",
        url: requestQuery,
        success: function (subjectItems) {
            //we are retrieving an array of arrays; each array with the datatype and data for example subject='slc'
            addSelectorElement(selector, elementId, defaultSelectorValue);
            $.each(subjectItems, function (rIndex, rItem) {
                let data = rItem[elementItemName];
                addSelectorElement(selector, elementId, data);
            })
        },
        error: function () {
            errorLog("Error loading json.");
        }
    });
}
