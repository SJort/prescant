<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--Fancy-->
    <link rel="shortcut icon" type="image/png" href="/images/favicon.png"/>

    <title>PreScant - about</title>


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="/css/bootstrap/bootstrap-grid.css">
    <link rel="stylesheet" href="/css/bootstrap/bootstrap-reboot.css">

    <!--Third party libraries-->
    <script src="/js/jquery-3.3.1.js"></script>
    <script src="/js/bootstrap/bootstrap.bundle.js"></script>

    <!--My scripts-->
    <script src="/js/base-devel.js"></script>
    <script src="/js/navigation-bar.js"></script>
    <script src="/test/testscript.js"></script>
    <script src="/js/percentage-presence-table.js"></script>


</head>
<body>
<nav id="navigationBar">
</nav>

<div class="container">
    <h1 class="display-3">Guide</h1>
    There are 5 types of presence. You can determine which type of presence you want to see by selecting values in the dropdown menus, while leaving others to the default 'All' option
    <ul>
        <li>
            <b>See the overall presence of each student. Can be used to see how good a school performs.</b><br>
            Leave all dropdown menus on 'All'.
        </li>
        <li>
            <b>See the presence of a certain subject. Can be used to see the overall attendance of a subject.</b><br>
            Only fill in the Subject.
        </li>
        <li>
            <b>See the presence of a certain subject of a certain class. Can be used to see the overall class attendance of a subject.</b><br>
            Only fill in the Subject and Class.
        </li>
        <li>
            <b>See the presence of a certain lesson, and edit where necessary.</b><br>
            Fill in all values.
        </li>
        <li>
            <b>See the presence of a certain class. Can be used for SLC teachers to see how well their class is doing. Click on a Student to see their performance of each Subject</b><br>
            Only fill in the Class.
        </li>
    </ul>
</div>


</body>
</html>



