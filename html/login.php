<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--Fancy-->
    <link rel="shortcut icon" type="image/png" href="/images/favicon.png"/>

    <title>PreScant - login</title>


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="/css/bootstrap/bootstrap-grid.css">
    <link rel="stylesheet" href="/css/bootstrap/bootstrap-reboot.css">

    <!--Third party libraries-->
    <script src="/js/jquery-3.3.1.js"></script>
    <script src="/js/bootstrap/bootstrap.bundle.js"></script>

    <!--My scripts-->
    <script src="/js/base-devel.js"></script>
    <script src="/js/navigation-bar.js"></script>
    <script src="/js/login.js"></script>

    <title>warm vogel - login</title>
</head>
<body>

<nav id="navigationBar">
</nav>

<div class="container">
    <!--i have no idea why but this dynamically positions the elements of flex-->
    <div class="d-flex flex-sm-row flex-column justify-content-center">
        <div class="p-2">
            <h1 class="display-3">Prescant</h1>

            <form id="loginForm" action="/php/login-server.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="loginField">Login</label>
                    <input type="text" name="username" class="form-control" id="loginField" placeholder="login">
                </div>
                <div class="form-group">
                    <label for="passwordField">Password</label>
                    <input type="password" name="password" class="form-control" id="passwordField"
                           placeholder="password">
                </div>
                <!--return onLoginButton should return false if you don't want the page to reload-->
                <!--<button name="submitLogin" value="login" onclick="return onLoginButton()" type="submit"-->
                <button name="submit" value="login" type="submit" class="btn btn-success">
                    Login
                </button>
            </form>
        </div>
    </div>
</div>


</body>
</html>
