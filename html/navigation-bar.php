<a class="navbar-brand" href="/index.php">PreScant</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">

        <li class="nav-item active">
            <a class="nav-link" href="/index.php">Home</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="https://sites.google.com/view/warmvogel/" target="_blank">About</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/html/registration.html">Registration page</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/html/help.php">Help</a>
        </li>
    </ul>

    <ul id="sessionBar" class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" id="loggedInAs"></a>
        </li>
        <li class="nav-item">
            <a class="nav-link">
                <?php
                session_start();
                echo "Welcome " . $_SESSION["username"];
                ?>
            </a>
        </li>
        <li class="nav-item">
            <button id="logoutButton" onclick="onLogoutButton()" type="button" class="btn btn-danger">Logout</button>
        </li>
    </ul>
</div>
