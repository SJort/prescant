<?php
$dsn = 'mysql:dbname=prescant;host=localhost;port=3306;charset=utf8';
$connection = new \PDO($dsn, "root", "root");

// throw exceptions wanneer je een SQL error krijgt
$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
// prevent emulation of prepared statements
$connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

//variabelen voor het loggen en inloggen...
$phoneID = 2;

$key = "";    //de knop die ingedrukt is
$currentTime = date("H:i:s", time());

$username = "";

$studentID = "0953492";
$setStudentID = "0953492";
$teacherID = "KRUGW";


$appLogCounter = 1;	//dit is het logID (tabel AppLogs)

//variabelen van de geklikte buttons (als je op klas 'ti2b' klikt wil je in het sql statement ti2b gebruiken)
$study = "ti";    //voor klas
$year = "2";        //voor klasSS
$_class = "b";    //klas a,b,c,d...
$class = $study . $year . $_class; //de volledige klas

$subject = "TINWIS";    //voor lesID (puur het vak)
$week = "6";        //voor lesID (de week)
$lesson = "1";    //voor lesID (de les in die week)
$lessonID = $subject . "-" . $week . "-" . $lesson;

$allSubjects = "TIN%";
$allLessons = $subject . "%";

$allStudyFromYearX = $study . $year . "%";
$allClassesFromStudy = $study . "%";

//dont repeat this over and over again every request...
//dat ik dit niet eerder heb opgemerkt...
//dit werkt niet... :(

//function executeStatement($statement)
//{
//	$statement->execute();
//	$results = $statement->fetchAll(PDO::FETCH_ASSOC);
 //	$json = json_encode($results);
//	echo $json;
//}


//functie om het ip adres van de ingelogde gebruiker op te halen
/*
function getUserIP()
{
	if(ISSET($_SERVER["HTTP_CF_CONNECTING_IP"))
	{
		$_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
		$_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
	}

	$client = @$_SERVER['HTTP_CLIENT_IP'];
	$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	$remote = $_SERVER['REMOTE_ADDR'];

	if(filter_var($client, FILTER_VALIDATE_IP))
	{
		$ip = $client;
	}

	else if(filter_var($forward, FILTER_VALIDATE_IP))
	{
		$ip = $forward;
	}

	else
	{
		$ip = $remote;
	}

	return $ip;
}
 */

//variabale
//$ip = getUserIP();


//hieronder allemaal requests
//Alle get requests

if($_GET["request"] == "getTotalAmountOfAllLessons")
{
	$allLessons = $subject . "%";
	$statement = $connection->prepare("select count(Presence.lessonID) from Presence where Presence.lessonID like 'TIN%'");
	$statement->execute();
	$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	$json = json_encode($result);
	echo $json;
}

if($_GET["request"] == "getAmountLessonFromSubX")
{
	$allLessons = $subject . "%";
	$statement = $connection->prepare("select subjectCode, count(lessonID) as aantal_lessen from Lesson where Lesson.lessonID like '".$allLessons."'");
	$statement->execute();
	$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	$json = json_encode($result);
	echo $json;
}

if($_GET["request"] == "getSubjectsFromTeachers")
{
	$statement = $connection->prepare("select distinct Lesson.subjectCode
					   from Lesson, Presence, Account, DetectionDevice
					   where DetectionDevice.phoneID = Presence.phoneID
					   and DetectionDevice.TeacherID = '".$teacherID."'
					   and Account.PersonID = '".$teacherID."'
					   and Account.PersonID = DetectionDevice.TeacherID
					   and Lesson.lessonID = Presence.lessonID
					   and Presence.phoneID = 
					   (select phoneID from DetectionDevice
					   where TeacherID = '".$teacherID."')");
	$statement->execute();
	$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	$json = json_encode($result);
	echo $json;
}

if($_GET["request"] == "getRole")
{
	$statement = $connection->prepare("select role from Person where Person.personID = '".$teacherID."'");
	$statement->execute();
	$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	$json = json_encode($result);
	echo $json;
}

if($_GET["request"] == "getNamesFromClassX")
{
	$statement = $connection->prepare("select distinct personID, name, infix, lastname
					   from Person, Student
					   where Person.personID in
					   (select studentID from Student
					   where class = '".$class."')");
	$statement->execute();
	$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	$json = json_encode($result);
	echo $json;
}

if($_GET["request"] == "getAllSubFromStudX")
{
	$allLessons = $subject . "%";
	$statement = $connection->prepare("select Lesson.subjectCode, Person.name, Person.infix, Person.lastname, count(Presence.LessonID) as aantal_keer_aanwezig
					   from Lesson, Presence, Student, Person
					   where Student.studentID = '".$studentID."'
					   and Student.studentID = Person.personID
					   and Presence.lessonID like '".$allLessons."'
					   and Lesson.subjectCode = '".$subject."'
					   and Student.studentID = Presence.studentID
					   and Lesson.lessonID = Presence.lessonID");
	$statement->execute();
	$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	$json = json_encode($result);
	echo $json;
}










//get requests voor de optieboxen van de website
  if(ISSET($_GET["year"]))
  {
    $year = $_GET["year"];
  }

  if(ISSET($_GET["study"]))
  {
    $study = $_GET["study"];
  }

  if(ISSET($_GET["class"]))
  {
	  $_class = $_GET["class"];
	  $class = $study . $year . $_class;
  }
   
  if(ISSET($_GET["subject"]))
  {
	  $subject = $_GET["subject"];
	  $lessonID = $subject . "-" . $week . "-" . $lesson;

  }
  
  if(ISSET($_GET["week"]))
  {
	  $week = $_GET["week"];
	  $lessonID = $subject . "-" . $week . "-" . $lesson;

  }
  
  if(ISSET($_GET["lesson"]))
  {
	  $lesson = $_GET["lesson"];
	  $lessonID = $subject . "-" . $week . "-" . $lesson;
  }
  
  if(ISSET($_GET["username"]))
  {
    $username = $_GET["username"];
  }
  
  if(ISSET($_GET["ip"]))
  {
    $ip = $_GET["ip"];
  }
  
  if(ISSET($_GET["phoneID"]))
  {
    $phoneID = $_GET["phoneID"];
  }
   
  if(ISSET($_GET["pressedKey"]))
  {
    $key = $_GET["pressedKey"];
  }

  if(ISSET($_GET["studentID"]))
  {
      $studentID = $_GET["studentID"];
//    $setStudentID = $_GET["studentID"];
//echo $setStudentID;
//    $studentID = $setStudentID;
  }

//get requests voor de mogelijkheden van de optieboxen op de website
if ($_GET["request"] == "getPersonID") {
    $statement = $connection->prepare("select personID from Person
									   where Person.personID = Account.PersonID
									   and Account.username = '" . $username . "'");

    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $json = json_encode($results);
    echo $json;
}

if ($_GET["request"] == "getAllStudy") {
    $statement = $connection->prepare("select studyName from Study");
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $json = json_encode($results);
    echo $json;
}

if ($_GET["request"] == "getAllYears") {
    $statement = $connection->prepare("select distinct substring(class, 3, 1) as year from Student");
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $json = json_encode($results);
    echo $json;
}

if ($_GET["request"] == "getAllClasses") {
    $statement = $connection->prepare("select distinct substring(class, 4, 1) as class from Student");
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $json = json_encode($results);
    echo $json;
}

if ($_GET["request"] == "getAllSubjects") {
    $statement = $connection->prepare("select distinct subjectCode as subject from Subject");
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $json = json_encode($results);
    echo $json;
}

//================================================================================================================
//get requests voor studenten aanwezigheids data
//een specifieke klas voor een specifieke les
if ($_GET["request"] == "getClassAttendanceFromCurr") {
	$statement = $connection->prepare("select distinct name, infix, lastname, personID, 
									   case when (name, infix, lastname, personID) in
									   (select name, infix, lastname, personID
	    								   from Person, Student, Lesson, Presence
									   where Person.personID = Student.studentID
									   and Presence.studentID = Student.studentID
									   and Student.class = '" . $class . "'
									   and Presence.lessonID = Lesson.lessonID
									   and Lesson.lessonID = '" . $lessonID ."')
									   then '1'
									   else '0'
									   end as isPresent
									   from Person, Student, Lesson, Presence
									   where Person.personID = Student.studentID
									   and Student.class = '" . $class . "'");
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $json = json_encode($results);
    echo $json;
}

//een specifieke student voor alle lessen van een bepaald vak
if ($_GET["request"] == "getStudentAttendanceFromAllLes") {
    $request = "select name, infix, lastname, count(personID) as aantalKeerAanwezig
									   from Person, Student, Lesson, Presence
									   where Person.personID = Student.studentID 
									   and Presence.studentID = Student.studentID
									   and Student.studentID = '" . $studentID . "'
									   and Presence.lessonID = Lesson.lessonID
									   and Lesson.lessonID like '" . $allLessons . "'
									   group by personID";
    $statement = $connection->prepare($request);
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $json = json_encode($results);
    echo $json;
}

//een specifieke klas voor alle lessen van een bepaald vak
if ($_GET["request"] == "getClassAttendanceFromAllLes") {
	$allLessons = $subject . "%";
    $statement = $connection->prepare("select name, infix, lastname, count(personID) as aantalKeerAanwezig 
									   from Person, Student, Lesson, Presence
									   where Person.personID = Student.studentID 
									   and Presence.studentID = Student.studentID
									   and Student.class = '" . $class . "'
									   and Presence.lessonID = Lesson.lessonID
									   and Lesson.lessonID like '" . $allLessons . "'
									   group by personID");
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $json = json_encode($results);
    echo $json;
}

//alle klassen van alle lessen van een vak
if ($_GET["request"] == "getAllClassAttendanceFromAllLes") {
	$allLessons = $subject . "%";
	$allStudyFromYearX = $study . $year . "%";
    $statement = $connection->prepare("select name, infix, lastname, count(personID) as aantalKeerAanwezig
									   from Person, Student, Lesson, Presence
									   where Person.personID = Student.studentID 
									   and Presence.studentID = Student.studentID
									   and Student.class like '" . $allStudyFromYearX . "'
									   and Presence.lessonID = Lesson.lessonID
									   and Lesson.lessonID like '" . $allLessons . "'
									   group by personID");
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $json = json_encode($results);
    echo $json;
}

//alle klassen van alle lessen van alle vakken
if ($_GET["request"] == "getAllClassAttendanceFromAllSub") {
	$allStudyFromYearX = $study . $year . "%";
    $statement = $connection->prepare("select name, infix, lastname, count(personID) as aantalKeerAanwezig
									   from Person, Student, Lesson, Presence
									   where Person.personID = Student.studentID 
									   and Presence.studentID = Student.studentID
									   and Student.class like '" . $allStudyFromYearX . "'
									   and Presence.lessonID = Lesson.lessonID
									   and Lesson.lessonID like '" . $allSubjects . "'
									   group by personID");
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $json = json_encode($results);
    echo $json;
}

//=========================================================================================================

//Alle post requests
//wat zou je allemaal moeten kunnen posten?

//alle loggin logs ->
//voor de docent, studenten handmatig aanwezig zetten, live database updaten.


/*

if($_POST["request"] == "logKey")
{
	$statement = connection->prepare("insert into LOGTABEL(userID, message, ip, time)
									 values('".$username."', 'key ".$key." pressed', ".$ip.", ".$currentTime.")");
	$statement->exec();
}
 */


/*

if($_POST["request"] == "logLoggin")
{
	$statement = connection->prepare("insert into AppLogs(LogID, userID, LogCodeID, time, ip)
									 values($appLogCounter, ".$studentID.", 1, ".$currentTime.", ".$ip.")");
	$statement->execute();
	$appLogCounter = $appLogCounter + 1;
}

if($_POST["request"] == "logLoggout")
{
	$statement = connection->prepare("insert into AppLogs(LogID, userID, LogCodeID, time, ip)
									 values($appLogCounter, '".$studentID."', 2, ".$currentTime.", ".$ip.")");
	$statement->execute();
	$appLogCounter = $appLogCounter + 1;
}

 */





if($_GET["request"] == "setPresence")
{
	$statement = $connection->prepare("insert into Presence(StudentID, lessonID,  phoneID, time)
					   values('".$studentID."', '".$lessonID."', $phoneID, '".$currentTime."')");
	$statement->execute();
	echo "execute done";
}

if($_GET["request"] == "deletePresence")
{

	$statement = $connection->prepare("delete from Presence
									 where Presence.StudentID = '".$studentID."'
									 and Presence.lessonID = '".$lessonID."'
									 and Presence.phoneID = $phoneID");

	$query = "  '".$studentID."', '".$lessonID."', $phoneID   ";
	echo $query;	
	$statement->execute();

}
 
?>
