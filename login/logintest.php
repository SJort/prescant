<?php
session_start();
$dsn = 'mysql:dbname=prescant;host=localhost;port=3306;charset=utf8';
$connection = new \PDO($dsn, "root", "root");

// throw exceptions, when SQL error is caused
$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
// prevent emulation of prepared statements
$connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

#login function, returns user_id if username and password match a entry
#does this by checking if result from query is empty or not,
#and on success it updates the user's lastLogin attribute to the current time
#GET request
#@Param username, password
#returns user_id if valid login, else corresponding error msg
if(isset($_POST["submit"])){
	//check if user sent a username and password
	if(isset($_POST['password'])){
		if(isset($_POST['username'])){
			//if so prepare query with username and password
			$statement = $connection->prepare("SELECT * FROM Account where username = :usernamep");
			$statement->bindParam(':usernamep',$_POST['username'],PDO::PARAM_STR);
			$statement->execute();
			$results = $statement->fetchAll(PDO::FETCH_ASSOC);
			$password = $results[0]['passwd'];

			//check if result is empty, meaning there is no match
			if(!password_verify($_POST['password'],$password)){
				echo 'error invalid login';
				//TO DO log login attempt
			}else {
				#update user's lastLogin attribute in database
				$userid = $results[0]["PersonID"];
				$statement = $connection->prepare("update Account set lastLogin = NOW() where PersonID = :useridp");
				$statement->bindParam(':useridp',$userid,PDO::PARAM_STR);
				$statement->execute();
				echo 'login success';
				$_SESSION['username']=$_POST['username'];
				$_SESSION['userid']=$userid;
				header('Location: landingpage.php');
				exit();
			}

		}else {echo 'error no username';}
	}else {echo 'error no password';}
}
?>






