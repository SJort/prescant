<?php
session_start();
$dsn = 'mysql:dbname=prescant;host=localhost;port=3306;charset=utf8';
$connection = new \PDO($dsn, "root", "root");

// throw exceptions, when SQL error is caused
$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
// prevent emulation of prepared statements
$connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

#login function, returns user_id if username and password match a entry
#does this by checking if result from query is empty or not,
#and on success it updates the user's lastLogin attribute to the current time
#GET request
#@Param username, password
#returns user_id if valid login, else corresponding error msg
if(isset($_POST["submit"])){
	//check if user sent a username and password
	if(isset($_POST['password'])){
		if(isset($_POST['username'])){
			//if so prepare query with username and password
			$statement = $connection->prepare("SELECT * FROM Account where username = :usernamep");
			$statement->bindParam(':usernamep',$_POST['username'],PDO::PARAM_STR);
			$statement->execute();
			$results = $statement->fetchAll(PDO::FETCH_ASSOC);
			$username = $results[0]['username'];

			if(isset($username)){
				echo "username already taken, please try again";
			}else {

				echo "insert into Person values('";
				echo $_POST['name'];				
				echo "', '";
				echo $_POST['infix'];
				echo "', '";
				echo $_POST['lastname'];
				echo "', '";
				echo $_POST['gender'];
				echo "', '";
				echo $_POST['role'];
				echo "', '";
				echo $_POST['userId'];
				echo "');";

				#make person
				$statement = $connection->prepare("insert into Person values(:name,:infix,:lastname,:gender,:role,:id)");
				$statement->bindParam(':id',$_POST['userId'],PDO::PARAM_STR);
				$statement->bindParam(':role',$_POST['role'],PDO::PARAM_STR);
				$statement->bindParam(':name',$_POST['name'],PDO::PARAM_STR);				
				$statement->bindParam(':infix',$_POST['infix'],PDO::PARAM_STR);
				$statement->bindParam(':lastname',$_POST['lastname'],PDO::PARAM_STR);
				$statement->bindParam(':gender',$_POST['gender'],PDO::PARAM_STR);
				$statement->execute();

				$password = password_hash($_POST['password'], PASSWORD_DEFAULT);
				
				echo "insert into Account values('";
				echo $_POST['username'];				
				echo "', '";
				echo $password;
				echo "',";
				echo $_POST['userId'];
				echo "', '";
				echo "NOW()";
				echo ");";
				 

				#make account 'Muhamed','','Agic','M','Student','0946536','1')
				$password = password_hash($_POST['password'], PASSWORD_DEFAULT);
				$statement = $connection->prepare("insert into Account values(:usern,:passw,:id,NOW())");
				$statement->bindParam(':usern',$_POST['username'],PDO::PARAM_STR);
				$statement->bindParam(':passw',$password,PDO::PARAM_STR);
				$statement->bindParam(':id',$_POST['userId'],PDO::PARAM_INT);
				$statement->execute();
				header('Location: /index.php');
				exit();
			}

		}else {echo 'error no username';}
	}else {echo 'error no password';}
}
?>






