# PreScant
The site of warm vogel:<br>
[warmvogel.nl](https://www.warmvogel.nl)<br>
Google Site can be found [here.](https://sites.google.com/view/warmvogel/)


# Site installation guide for Arch Linux

### Install required packages
```shell
sudo pacman -S apache
sudo pacman -S mariadb
sudo pacman -S php
sudo pacman -S php-apache
```

### Clone git repository
```shell
git clone git@gitlab.com:SJort/prescant.git
```
https://wiki.archlinux.org/index.php/Apache_HTTP_Server#User_directories
<br>Setup access rights for home directory and cloned folder:
```shell
chmod o+x -R ~
chmod o+x -R ~/this/prescant
```

### Setup apache
https://wiki.archlinux.org/index.php/Apache_HTTP_Server
<br>Edit apache configuration file:
```shell
sudo vim /etc/httpd/conf/httpd.conf
```
1. Change DocumentRoot and Directory to the cloned git folder, prefacing it with /
2. Comment line: '#LoadModule mpm_event_module modules/mod_mpm_event.so'
3. Uncomment line: 'LoadModule mpm_prefork_module modules/mod_mpm_prefork.so'
4. Add to the end of the LoadModule list: 'LoadModule php7_module modules/libphp7.so' 
5. Add to the end of the LoadModule list: 'AddHandler php7-script .php'
6. Add to the end of the Include list: 'Include conf/extra/php7_module.conf'
7. Change None in AllowOverride under our DocumentRoot to All to enable .htaccess

Enable and start the httpd service:
```shell
sudo systemctl enable --now httpd
```
The site should now be accessible with limited functionality.

### Setup PHP
https://wiki.archlinux.org/index.php/PHP#MySQL/MariaDB
<br>Edit PHP configuration file:
```shell
sudo vim /etc/php/php.ini
```
1. Uncomment line 'extension=pdo_mysql'
2. Uncomment line 'extension=mysqli'

### Setup MariaDB
https://www.ostechnix.com/how-to-reset-mysql-or-mariadb-root-password/
<br>Execute this command without thinking:
```shell
sudo mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
```
Enable and start the mariadb service:
```shell
sudo systemctl enable --now mariadb
```
Login:
```shell
mysql -u root
```
Change the root password to root:
```sql
UPDATE mysql.user SET Password=PASSWORD('root') WHERE User='root';
FLUSH PRIVILEGES;
exit
```
Now you can login in SQL with:
```shell
mysql -u root -proot
```
Paste the database creation queries there, which you can find in this repository.

### Setup HTTPS with Cloudflare
Generate a pem and private key on Cloudflare.

Save the pem key here:
```shell
sudo vim /etc/httpd/conf/server.crt
```
Save the private key here:
```shell
sudo vim /etc/http/conf/server.key
```
Edit the apache configuration file:
```shell
sudo vim /etc/httpd/conf/httpd.conf
```
1. Under 'VirtualHost _default_:443' update the path in DocumentRoot to '<path_to_document>'
2. Uncomment 'LoadModule ssl_module modules/mod_ssl.so'
3. Uncomment 'LoadModule socache_shmcb_module modules/mod_socache_shmcb.so'
4. Uncomment 'Include conf/extra/httpd-ssl.conf'
