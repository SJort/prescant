package nl.warmvogel.androidapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;

public class SelectActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private ListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //default setup shit
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);

        //create ArrayList with Item's to fill the RecyclerView with
        final ArrayList<Item> itemList = new ArrayList<>();
        itemList.add(new Item(R.drawable.ic_chevron_right, "Line 1", "Line 2"));
        itemList.add(new Item(R.drawable.ic_chevron_right, "Line 3", "Line 4"));
        itemList.add(new Item(R.drawable.ic_chevron_right, "Line 5", "Line 6"));

        //fill the shit, the onClickListener is handled within this method, see comment there
        fillRecyclerView(itemList);
    }

    public void fillRecyclerView(final ArrayList<Item> items){
        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new ListAdapter(items);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new ListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Item item = items.get(position);

                //WHAT TO DO WITH THE CLICKED ITEM
                Toast.makeText(getApplicationContext(), item.getText1() + " clicked!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
