<?php
include_once "php/login-check.php";
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <!--Fancy-->
    <link rel="shortcut icon" type="image/png" href="/images/favicon.png"/>

    <title>PreScant - warm vogel</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="/css/bootstrap/bootstrap-grid.css">
    <link rel="stylesheet" href="/css/bootstrap/bootstrap-reboot.css">

    <!--My CSS-->
    <link rel="stylesheet" href="/css/styles.css">

    <!--Third party libraries-->
    <script src="/js/jquery-3.3.1.js"></script>
    <script src="/js/bootstrap/bootstrap.bundle.js"></script>
    <!--<script src="js/bootstrap.js"></script>-->
    <!--Exclude this, otherwise dropdown menus will need two clicks to open...-->

    <!--My scripts-->
    <script src="/js/base-devel.js"></script>
    <script src="/js/navigation-bar.js"></script>
    <script src="/js/selectors.js"></script>
    <script src="/js/presence-table.js"></script>
    <script src="/js/manual-presence-table.js"></script>
    <script src="/js/percentage-presence-table.js"></script>
    <script src="/js/student-list-table.js"></script>
    <script src="/js/script.js"></script>

</head>

<body>
<nav id="navigationBar">
</nav>

<div class="container">
    <!--i have no idea why but this dynamically positions the elements of flex-->
    <div id="selectorHolder" class="d-flex flex-sm-row flex-column justify-content-center">
        <div class="p-2">
            <h5>Subject</h5>
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false" id="subjectSelectorButton">
                    All
                </button>
                <div class="dropdown-menu" id="subjectSelector">
                </div>
            </div>
        </div>

        <div class="p-2">
            <h5>Class</h5>
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false" id="classSelectorButton">
                    All
                </button>
                <div class="dropdown-menu" id="classSelector">
                </div>
            </div>
        </div>
        <div class="p-2">
            <h5>Week</h5>
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false" id="weekSelectorButton">
                    All
                </button>
                <div class="dropdown-menu" id="weekSelector">
                </div>
            </div>
        </div>
        <div class="p-2">
            <h5>Lesson</h5>
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false" id="lessonSelectorButton">
                    All
                </button>
                <div class="dropdown-menu" id="lessonSelector">
                </div>
            </div>
        </div>
        <div class="p-2">
            <h5 style="opacity: 0">Load</h5>
            <button type="button" class="btn btn-success" onclick="loadTable()">
                Reload
            </button>
        </div>
    </div>

    <table class="table table-striped" id="presenceTable">
    </table>

    <hr/>

    <h3 class="card-title">Log</h3>
    <div class="card">
        <div class="card-body" id="logBar"></div>
    </div>

</div>
</body>
</html>
