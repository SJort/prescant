<?php
$dsn = 'mysql:dbname=testdb;host=localhost;port=3306;charset=utf8';
$connection = new \PDO($dsn, "root", "root");

// throw exceptions, when SQL error is caused
$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
// prevent emulation of prepared statements
$connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

#returns a list of all people in json format
#GET request
if ($_GET["request"] == "getPresence") {
    $statement = $connection->prepare("SELECT * FROM presence");
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $json = json_encode($results);

    echo $json;
}

$subject = "noSubject";
$class = "noClass";

#url: http://localhost/testdb.php?request=setSelector&subject=4
if ($_GET["request"] == "set") {
    if (ISSET($_GET["subject"])) {
        $subject = $_GET["subject"];
        echo($subject . "<br>");
    }

    if(ISSET($_GET["class"])){
        $class = $_GET["class"];
        echo($class ."<br>");
    }
}


#input: updatePresence?studentId=0953492&present=1
if ($_GET["request"] == "updatePresence") {
    if (ISSET($_GET["studentId"])) {
        if (ISSET($_GET["present"])) {
            $statement = $connection->prepare("update presence set present = :pres where studentId = :stud");
            $statement->bindParam(':pres', $_GET["present"], PDO::PARAM_INT);
            $statement->bindParam(':stud', $_GET["studentId"], PDO::PARAM_INT);
            $statement->execute();
        }
    }
}
?>






