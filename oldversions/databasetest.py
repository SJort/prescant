import tkinter as tk
import tkinter.ttk as TTK
from tkinter import *
from tkinter.ttk import *
from DatabaseConnection import *
from datetime import datetime

dbc = DatabaseConnection('root', 'Muhammed123', 'presentiehro', 'localhost')
currentTime = datetime.now().strftime('%y-%m-%d %H:%M:%S')
print(currentTime)
#dbc.executeStatement(f"insert into ApplicationLogs(logmessage,tijdstip) values('User went to manual presence registration', '{currentTime}')")

class GUI_Start(tk.Tk):
	def __init__(self):
		tk.Tk.__init__(self)
		self._frame = None
		self.switchFrame(StartPage)

	def switchFrame(self, frameClass):
		newFrame = frameClass(self)
		#als er een frame is, destroy hem
		if self._frame is not None:
			self._frame.destroy()
		self._frame = newFrame
		self._frame.pack()

		
class StartPage(tk.Frame):
	def __init__(self, master):
		tk.Frame.__init__(self, master)
		mainLabel = tk.Label(self, text = "HRO Presentie systeem")
		mainLabel.grid(row = 0, column = 5)
		
		manualReg = tk.Button(self, text = "Handmatige Aanwezigheidsregistratie", command = lambda: [master.switchFrame(ManualRegistrationPage), self.execStatement(f"insert into ApplicationLogs(logmessage,tijdstip) values('User went to manual presence registration', '{currentTime}')")])
		manualReg.grid(row = 2, column = 5)
		
	def execStatement(self, statement):
		dbc.executeStatement(statement)
		
		
class ManualRegistrationPage(tk.Frame):
	def __init__(self, master):
		tk.Frame.__init__(self, master)
		
		goBack = tk.Button(self, text = 'terug', command = lambda: master.switchFrame(StartPage))
		goBack.grid(row = 11, column = 1)
		
		manRegInfoLabel = tk.Label(self, text = "Selecteer een klas")
		manRegInfoLabel.grid(row = 3, column = 5)
		
		#een lijst van alle klassen die je in de student tabel kan vinden
		#je krijgt dus alle klassen terug (1x door de distinct)
		
		conceptIdeeLabel = tk.Label(self, text = 'De klas hoort hier te laden, maar voor nu...')
		conceptIdeeLabel.grid(row = 7, column = 5)
		
		idInputInstructionLabel = tk.Label(self, text = 'Vul het studentnummer in')
		idInputInstructionLabel.grid(row = 8, column = 5)
		
		self.choices = tk.StringVar()                         
		self.choices = ttk.Combobox(self, width = 12, textvariable = self.choices)
		self.choices['values'] = ('ti2a', 'ti2b')     
		self.choices.grid(row = 3, column = 6)              
		self.choices.current(0)
		
		self.idInput = tk.Entry(self)
		self.idInput.grid(row = 8, column = 6)
		
		confirmAll = tk.Button(self, text = 'bevestigen', command = lambda: [self.confirmAndWriteData(), master.switchFrame(StartPage)])
		confirmAll.grid(row = 9, column = 5)
		
	def confirmAndWriteData(self):
		klas = self.choices.get()
		personID = self.idInput.get()
		
		dbc.executeStatement(f'insert into Presence(lessonID,studentID,phoneID,time) values(1,"{personID}",1,"{currentTime}")')
		dbc.executeStatement(f'insert into ApplicationLogs(logmessage,tijdstip) values("Person {personID} present gemeld", "{currentTime}")')
		
		'''
		getTimeLastInsertedRow = dbc.executeStatement(f'select time from presence where studentID = "{personID}" order by id desc limit 1')
		
		#controle om te kijken of de data ook echt in de database is gezet
		if(currentTime - getTimeLastInsertedRow > 3):
			print('er is iets mis gegaan')
			errorLabel = tk.Label(self, text = 'Er is iets misgegaan, excuses voor het ongemak, probeer het later opnieuw')
			errorLabel.grid(row = 11, column = 5)
	'''

if __name__ == "__main__":
	app = GUI_Start()
	app.mainloop()