<?php
$dsn = 'mysql:dbname=presentiehro;host=localhost;port=3306;charset=utf8';
$connection = new \PDO($dsn, "root", "root");

// throw exceptions, when SQL error is caused
$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
// prevent emulation of prepared statements
$connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

#returns a list of all people in json format
#GET request
if($_GET["request"] == "getPersons"){
	if(isset($_GET['lessonId'])){
		$statement = $connection->prepare("SELECT * FROM Person");
		$statement->execute();
		$results = $statement->fetchAll(PDO::FETCH_ASSOC);
		$json = json_encode($results);

		echo $json;
	}
}

if($_GET["request"] == "isPresent"){
	if(isset($_GET['lessonId'])){
		if(isset($_GET['studentId'])){
			$statement = $connection->prepare("select exists(select * from Presence  where Presence.lessonID = :lesson and Presence.StudentID = :student) as presence");
			$statement->bindParam(':lesson',$_GET["lessonId"],PDO::PARAM_INT);
			$statement->bindParam(':student',$_GET["studentId"],PDO::PARAM_INT);
			$statement->execute();
			$results = $statement->fetchAll(PDO::FETCH_ASSOC);
			$json = json_encode($results);
			echo $json;
		}
	}
}


?>






