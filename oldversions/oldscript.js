
function loadSampleParsed() {
    log("Loading sample parsed table!");
    let table = document.getElementById("SampleParsed"); //jquery way doesn't work, function like insertRow not recognized
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: 'https://jsonplaceholder.typicode.com/todos',
        success: function (rowItems) {
            let currentRow;
            let currentCell;
            //we are retrieving an array of arrays; the rows with for each row the column items
            $.each(rowItems, function (rowIndex, rowItem) {
                table.insertRow();
                currentRow = table.rows[table.rows.length - 1];
                $.each(rowItem, function (columnIndex, columnItem) {
                    currentRow.insertCell();
                    currentCell = currentRow.cells[currentRow.cells.length - 1];
                    currentCell.innerText = columnItem;
                });
            })
        },
        error: function () {
            errorLog("Error loading sample json.");
        }
    });
}

function addRow() {
    const table = document.getElementById("presenceTableBody");
    table.insertRow();

    const rowIndex = table.rows.length - 1;
    const currentRow = table.rows[rowIndex];

    for (let column = 0; column < 4; column++) {
        currentRow.insertCell();
    }

    currentRow.cells[0].innerText = "Name " + rowIndex;
    currentRow.cells[1].innerText = (rowIndex + 1) * 953492;
    let checkbox = document.createElement('input');
    checkbox.setAttribute("type", "checkbox");
    currentRow.cells[3].innerText = "modified";
    currentRow.cells[3].setAttribute("style", "opacity: 0");

    //random data for testing purposes
    if (Math.random() >= 0.5) {
        checkbox.setAttribute("originalValue", "0");
    } else {
        checkbox.setAttribute("originalValue", "1");
    }

    //set checkbox checked if original value is also checked
    checkbox.checked = checkbox.getAttribute("originalValue") === "1";

    //events are going to our custom class, so we have to do an alternative way to catch them
    $(checkbox).on("change", function () {
        log("Checkbox at row " + rowIndex + " changed to " + this.checked);
        //if the original value matches the current value
        if (checkbox.getAttribute("originalValue") === "1" && this.checked || checkbox.getAttribute("originalValue") === "0" && !this.checked) {
            table.rows[rowIndex].cells[3].setAttribute("style", "opacity: 0");
        }
        //else give a warning the entry is modified
        else {
            //we can't just insert a 'p' tag with that style, because that makes the row extend in length
            //also using rowIndex instead of currentRow because currentRow somehow referred to the last row
            table.rows[rowIndex].cells[3].setAttribute("style", "opacity: 1");
            table.rows[rowIndex].cells[3].className = "text-warning";
        }
    });
    currentRow.cells[2].appendChild(checkbox);
    log("Added row with index " + rowIndex);
}
