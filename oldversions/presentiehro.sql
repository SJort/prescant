-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: presentiehro
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `applicationaccount`
--

DROP TABLE IF EXISTS `applicationaccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicationaccount` (
  `accountID` int(11) NOT NULL,
  `username` varchar(512) DEFAULT NULL,
  `passwd` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicationaccount`
--

LOCK TABLES `applicationaccount` WRITE;
/*!40000 ALTER TABLE `applicationaccount` DISABLE KEYS */;
/*!40000 ALTER TABLE `applicationaccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applicationlogs`
--

DROP TABLE IF EXISTS `applicationlogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicationlogs` (
  `logmessage` varchar(255) DEFAULT NULL,
  `tijdstip` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicationlogs`
--

LOCK TABLES `applicationlogs` WRITE;
/*!40000 ALTER TABLE `applicationlogs` DISABLE KEYS */;
INSERT INTO `applicationlogs` VALUES ('User went to manual presence registration','2019-03-26 00:25:54'),('User went to manual presence registration','2019-03-26 00:28:06'),('User went to manual presence registration','2019-03-26 00:29:16'),('User went to manual presence registration','2019-03-26 00:30:01'),('User went to manual presence registration','2019-03-26 00:30:22'),('Person 0946536 present gemeld','2019-03-26 00:30:22'),('User went to manual presence registration','2019-03-26 00:31:10'),('Person 0946536 present gemeld','2019-03-26 00:31:10'),('User went to manual presence registration','2019-03-26 00:32:09'),('Person 0946536 present gemeld','2019-03-26 00:32:09'),('User went to manual presence registration','2019-03-26 10:46:14'),('Person 0946536 present gemeld','2019-03-26 10:46:14'),('User went to manual presence registration','2019-03-26 12:39:48'),('Person 0946536 present gemeld','2019-03-26 12:39:48');
/*!40000 ALTER TABLE `applicationlogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detectiondevice`
--

DROP TABLE IF EXISTS `detectiondevice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detectiondevice` (
  `phoneID` varchar(255) NOT NULL,
  `ownerID` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detectiondevice`
--

LOCK TABLES `detectiondevice` WRITE;
/*!40000 ALTER TABLE `detectiondevice` DISABLE KEYS */;
/*!40000 ALTER TABLE `detectiondevice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docent`
--

DROP TABLE IF EXISTS `docent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docent` (
  `docentID` int(11) NOT NULL,
  `teacherAbbrev` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docent`
--

LOCK TABLES `docent` WRITE;
/*!40000 ALTER TABLE `docent` DISABLE KEYS */;
/*!40000 ALTER TABLE `docent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lesson`
--

DROP TABLE IF EXISTS `lesson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lesson` (
  `lessonID` varchar(45) DEFAULT NULL,
  `subjectCode` varchar(45) DEFAULT NULL,
  `classroom` varchar(45) DEFAULT NULL,
  `starttime` time DEFAULT NULL,
  `endtime` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lesson`
--

LOCK TABLES `lesson` WRITE;
/*!40000 ALTER TABLE `lesson` DISABLE KEYS */;
INSERT INTO `lesson` VALUES ('TINDTB02-1-1','TINDTB02','WD.05.025','13:00:00','14:00:00'),('TINDTB02-2-1','TINDTB02','WD.05.025','13:00:00','14:00:00'),('TINDTB02-3-1','TINDTB02','WD.05.025','13:00:00','14:00:00'),('TINNET-1-1','TINNET','WD.05.002','14:00:00','15:00:00'),('TINNET-2-1','TINNET','WD.05.002','14:00:00','15:00:00'),('TINNET-3-1','TINNET','WD.05.002','14:00:00','15:00:00'),('TINWIS-1-1','TINWIS','WD.05.013','15:00:00','16:00:00'),('TINWIS-1-2','TINWIS','WD.05.013','15:00:00','16:00:00'),('TINWIS-2-1','TINWIS','WD.05.013','15:00:00','16:00:00'),('TINWIS-2-2','TINWIS','WD.05.013','15:00:00','16:00:00'),('TINWIS-3-1','TINWIS','WD.05.013','15:00:00','16:00:00');
/*!40000 ALTER TABLE `lesson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Person`
--

DROP TABLE IF EXISTS `Person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Person` (
  `firstname` varchar(45) DEFAULT NULL,
  `infix` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  `personID` varchar(45) DEFAULT NULL,
  `accountID` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Person`
--

LOCK TABLES `Person` WRITE;
/*!40000 ALTER TABLE `Person` DISABLE KEYS */;
INSERT INTO `Person` VALUES ('Muhamed','','Agic','M','Student','0946536','1'),('Jort','','Stuyt','M','Student','0953492','3'),('Guido','','Ponson','M','Student','0950071','4'),('Krzysztof','','Cajler','M','Student','0951562','5'),('Jan','t','proefkonijn','M','Student','0912345','6'),('Marieke','t','proefkonijn','V','Student','0954321','7');
/*!40000 ALTER TABLE `Person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `presence`
--

DROP TABLE IF EXISTS `presence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `presence` (
  `lessonID` varchar(45) DEFAULT NULL,
  `studentID` varchar(45) DEFAULT NULL,
  `phoneID` varchar(45) DEFAULT NULL,
  `time` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `presence`
--

LOCK TABLES `presence` WRITE;
/*!40000 ALTER TABLE `presence` DISABLE KEYS */;
INSERT INTO `presence` VALUES ('TINNET-1-1','0946536','2','14:00:00'),('TINNET-2-1','0946536','2','14:00:00'),('TINNET-3-1','0946536','2','14:00:00'),('TINNET-1-1','0953492','2','14:00:00'),('TINNET-2-1','0953492','2','14:00:00'),('TINNET-3-1','0953492','2','14:00:00'),('TINWIS-1-1','0953492','3','15:00:00'),('TINWIS-1-2','0953492','3','15:00:00'),('TINWIS-2-2','0953492','3','15:00:00'),('TINWIS-3-2','0953492','3','15:00:00'),('TINWIS-1-1','0946536','3','15:00:00'),('TINWIS-1-2','0946536','3','15:00:00'),('TINWIS-2-1','0946536','3','15:00:00'),('TINWIS-2-2','0946536','3','15:00:00'),('TINWIS-3-1','0946536','3','15:00:00'),('TINWIS-3-2','0946536','3','15:00:00'),('TINDTB02-1-1','0946536','1','13:00:00'),('TINDTB02-2-1','0946536','1','13:00:00'),('TINDTB02-3-1','0946536','1','13:00:00'),('TINNET-1-1','0912345','2','14:00:00'),('TINNET-2-1','0912345','2','14:00:00'),('TINNET-2-1','0954321','2','14:00:00'),('TINNET-1-1','0954321','2','14:00:00'),('TINNET-3-1','0954321','2','14:00:00');
/*!40000 ALTER TABLE `presence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `studentID` varchar(45) DEFAULT NULL,
  `class` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES ('0946536','ti2b'),('0953492','ti2b'),('0950071','ti2b'),('0951562','ti2b'),('0912345','ti2a'),('0954321','ti2a');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject`
--

DROP TABLE IF EXISTS `subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subject` (
  `subjectCode` varchar(45) NOT NULL,
  `subjectName` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject`
--

LOCK TABLES `subject` WRITE;
/*!40000 ALTER TABLE `subject` DISABLE KEYS */;
INSERT INTO `subject` VALUES ('TINDTB02','Databases'),('TINNET','Netwerken'),('TINWIS','Wiskunde');
/*!40000 ALTER TABLE `subject` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-13 16:08:55
