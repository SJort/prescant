<?php
$dsn = 'mysql:dbname=presentiehro;host=localhost;port=3306;charset=utf8';
$connection = new \PDO($dsn, "root", "root");

// throw exceptions wanneer je een SQL error krijgt
$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
// prevent emulation of prepared statements
$connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

//variabelen van de geklikte buttons (als je op klas 'ti2b' klikt wil je in het sql statement ti2b gebruiken)
$study = "";	//voor klas
$year = "";		//voor klas
$_class = "";	//klas a,b,c,d...
$class = "$study$year$_class"; //de volledige klas

$studentID = "";//studentID

$subject = "";	//voor lesID (puur het vak)
$week = "";		//voor lesID (de week)
$lesson = "";	//voor lesID (de les in die week)
$lessonID = "$subject$week$lesson";

$allSubjects = "$subject" . "%";
$allStudyFromYearX = "$study" . "$year" . "%";
$allClassesFromStudy = "$study" . "%";


//Alle get requests
//get requests voor de mogelijkheden van de optieboxen op de website
if($_GET["request"] == "getAllStudy")
{
	$statement = $connection->prepare("select distinct substring(class, 1, 2) from Student");
	$statement->execute();
	$results = $statement->fetchAll(PDO::FETCH_ASSOC);
	$json = json_encode($results);
	echo $json;
}

if($_GET["request"] == "getAllYears")
{
	$statement = $connection->prepare("select distinct substring(class, 3, 1) from Student");
	$statement->execute();
	$results = $statement->fetchAll(PDO::FETCH_ASSOC);
	$json = json_encode($results);
	echo $json;
}

if($_GET["request"] == "getAllclasses")
{
	$statement = $connection->prepare("select distinct substring(class, 4, 1) from Student");
	$statement->execute();
	$results = $statement->fetchAll(PDO::FETCH_ASSOC);
	$json = json_encode($results);
	echo $json;
}

if($_GET["request"] == "getAllSubjects")
{
	$statement = $connection->prepare("select distinct subjectName from Subject");
	$statement->execute();
	$results = $statement->fetchAll(PDO::FETCH_ASSOC);
	$json = json_encode($results);
	echo $json;
}

//================================================================================================================

//get requests voor studenten aanwezigheids data
//een specifieke klas voor een specifieke les
if($_GET["request"] == "getClassAttendanceFromCurr")
{
	$statement = $connection->prepare("select distinct firstname, infix, lastname, personID
									   from Person, Student, Lesson, presence
									   where Person.personID = Student.studentID 
									   and presence.studentID = Student.studentID
									   and Student.class = '$class'
									   and presence.lessonID = Lesson.lessonID
									   and Lesson.lessonID = '$lessonID'");
	$statement->execute();
	$results = $statement->fetchAll(PDO::FETCH_ASSOC);
	$json = json_encode($results);
	echo $json;
}

//een specifieke student voor alle lessen van een bepaald vak
if($_GET["request"] == "getStudentAttendanceFromAllLes")
{
	$statement = $connection->prepare("select firstname, infix, lastname, count(personID) 
									   from Person, Student, Lesson, presence
									   where Person.personID = Student.studentID 
									   and presence.studentID = Student.studentID
									   and Student.studentID = '$studentID'
									   and presence.lessonID = Lesson.lessonID
									   and Lesson.lessonID like '$allSubjects'
									   group by personID");
	$statement->execute();
	$results = $statement->fetchAll(PDO::FETCH_ASSOC);
	$json = json_encode($results);
	echo $json;
}

//een specifieke klas voor alle lessen van een bepaald vak
if($_GET["request"] == "getClassAttendanceFromAllLes")
{
	$statement = $connection->prepare("select firstname, infix, lastname, count(personID) 
									   from Person, Student, Lesson, presence
									   where Person.personID = Student.studentID 
									   and presence.studentID = Student.studentID
									   and Student.class = '$class'
									   and presence.lessonID = Lesson.lessonID
									   and Lesson.lessonID like '$allSubjects'
									   group by personID");
	$statement->execute();
	$results = $statement->fetchAll(PDO::FETCH_ASSOC);
	$json = json_encode($results);
	echo $json;
}

//alle klassen van alle lessen van een vak
if($_GET["request"] == "getAllClassAttendanceFromAllLes")
{
	$statement = $connection->prepare("select firstname, infix, lastname, count(personID) 
									   from Person, Student, Lesson, presence
									   where Person.personID = Student.studentID 
									   and presence.studentID = Student.studentID
									   and Student.class like '$allStudyFromYearX'
									   and presence.lessonID = Lesson.lessonID
									   and Lesson.lessonID like '$allSubjects'
									   group by personID");
	$statement->execute();
	$results = $statement->fetchAll(PDO::FETCH_ASSOC);
	$json = json_encode($results);
	echo $json;
}

//alle klassen van alle lessen van alle vakken
if($_GET["request"] == "getAllClassAttendanceFromAllSub")
{
	$statement = $connection->prepare("select firstname, infix, lastname, count(personID) 
									   from Person, Student, Lesson, presence
									   where Person.personID = Student.studentID 
									   and presence.studentID = Student.studentID
									   and Student.class like '$allStudyFromYearX'
									   and presence.lessonID = Lesson.lessonID
									   and Lesson.lessonID like '$allSubjects'
									   group by personID");
	$statement->execute();
	$results = $statement->fetchAll(PDO::FETCH_ASSOC);
	$json = json_encode($results);
	echo $json;
}
