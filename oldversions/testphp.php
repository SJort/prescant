<?php

function login() {
    //check if user sent a username and password
    if(isset($_POST['password'])){
        if(isset($_POST['username'])){
            $dsn = 'mysql:dbname=prescant;host=localhost;port=3306;charset=utf8';
            $connection = new \PDO($dsn, "root", "root");
            // throw exceptions, when SQL error is caused
            $connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            // prevent emulation of prepared statements
            $connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
            $statement = $connection->prepare("SELECT * FROM Account where username = :usernamep");
            $statement->bindParam(':usernamep',$_POST['username'],PDO::PARAM_STR);
            $statement->execute();
            $results = $statement->fetchAll(PDO::FETCH_ASSOC);
            $password = $results[0]['passwd'];

            //check if result is empty, meaning there is no match
            if(!password_verify($_POST['password'],$password)){
                echo 'error invalid login';
                //TO DO log login attempt
            }else {
                $userid = $results[0]["personID"];

                #check if user is a teacher
                $statement = $connection->prepare("select role from Person where Person.personID = :useridp");
                $statement->bindParam(':useridp',$userid,PDO::PARAM_STR);
                $statement->execute();
                $results = $statement->fetchAll(PDO::FETCH_ASSOC);
                $role = $results[0]['role'];

                if($role == "docent" || $role == "Docent"){
                    #update user's lastLogin attribute in database
                    $statement = $connection->prepare("update Account set lastLogin = NOW() where PersonID = :useridp");
                    $statement->bindParam(':useridp',$userid,PDO::PARAM_STR);
                    $statement->execute();


                    session_start();
                    $_SESSION['username']=$_POST['username'];
                    $_SESSION['userid']=$userid;
                    echo session_id();
                }else{
                    echo "error not a teacher";
                }
            }

        }else {echo 'error no username';}
    }else {echo 'error no password';}
}

function getLessons($username) {
    $dsn = 'mysql:dbname=prescant;host=localhost;port=3306;charset=utf8';
    $connection = new \PDO($dsn, "root", "root");
    $connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    $connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
    $statement = $connection->prepare("Select distinct Lesson.lessonID From Lesson, Presence, DetectionDevice Where DetectionDevice.phoneID = Presence.phoneID And DetectionDevice.TeacherID = :usernamep And Lesson.lessonID = Presence.lessonID");
    $statement->bindParam(':usernamep',$username,PDO::PARAM_STR);
    $statement->execute();
    $results = $statement->fetchColumn(0);
    while($results!=NULL){
        echo $results;
        echo " ";
        $results = $statement->fetchColumn(0);
    }
}

function present() {
    if(isset($_POST['uid'])){
        if(isset($_POST['subject'])){
            $dsn = 'mysql:dbname=prescant;host=localhost;port=3306;charset=utf8';
            $connection = new \PDO($dsn, "root", "root");
            $connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
            $statement = $connection->prepare("insert into Presence values (:subj, (select studentID from Cards where cardID = :uidp), 1, CURRENT_TIME())");
            $statement->bindParam(':subj',$_POST['uid'],PDO::PARAM_STR);
            $statement->bindParam(':uidp',$_POST['subject'],PDO::PARAM_STR);
            $statement->execute();
            echo "aanwezig";
        }else {echo "error no subject";}
    }else {echo "error no uid";}
}


if(isset($_POST['sessionid'])){
    session_id($_POST['sessionid']);
    session_start();
    if(isset($_SESSION['username'])){
        if(isset($_POST['lessons'])){
            getLessons($_SESSION['username']);
        }else{
            present();
        }
    }else{
        echo "error no login";
        session_unset();
        session_destroy();

        setcookie("PHPSESSID", "", 1);
        session_start();
        login();

    }
}else{
    session_start();
    login();
}


if($_GET["request"]=="logout"){
    session_unset();
    session_destroy();

    //this does not work, using the javascript redirect instead
    //header('Location: ../html/login.php');
    exit();
}
?>
