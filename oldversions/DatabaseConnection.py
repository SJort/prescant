from mysql import connector

class DatabaseConnection:
	def __init__(self, user, passwd, dtb, host):
		self.connection = connector.Connect(user = f'{user}', password = f'{passwd}' , database = f'{dtb}', host = f'{host}')
		
	def executeStatement(self, statement):
		self.cursor = self.connection.cursor()
		self.cursor.execute(statement)
		self.connection.commit()
		self.cursor.close()
		