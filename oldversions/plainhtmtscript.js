let count = 0;

function onLoad() {
    console.log("On load!");
    // loadTable()
}

function loadTable() {
    for (let row = 0; row < 4; row++) {
        addRow()
    }
}

function addRow() {
    const table = document.getElementById("presenceTableBody");
    table.insertRow();

    const rowIndex = table.rows.length - 1;
    const currentRow = table.rows[rowIndex];
    let columnIndex;
    let currentColumn;

    for (let column = 0; column < 4; column++) {
        currentRow.insertCell();
        columnIndex = currentRow.cells.length - 1;
        currentColumn = currentRow.cells[columnIndex];
        currentColumn.innerText = "Column " + columnIndex
    }
}
