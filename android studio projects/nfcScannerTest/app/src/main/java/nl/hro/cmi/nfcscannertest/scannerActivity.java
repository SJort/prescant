package nl.hro.cmi.nfcscannertest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static nl.hro.cmi.nfcscannertest.scannerActivity.TAG;

public class scannerActivity extends Activity {
    public static final String TAG = "scannerScreen";
    final static String sessionid = MainActivity.sessionId;
    final static String subject = MainActivity.subject;

    public final static String url = "https://www.warmvogel.nl/php/tes.php";
    public static Context jort;

    public static TextView mTextView;
    private NfcAdapter mNfcAdapter;

    public scannerActivity(){

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        cardreaderp mcardreaderp = new cardreaderp();
        setTitle("PreScant");
        jort = this;

        Log.i(TAG,sessionid);
        mTextView = (TextView) findViewById(R.id.textView_explanation);

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        mNfcAdapter.enableReaderMode(this,mcardreaderp, 0x00000003,null);

        if (mNfcAdapter == null) {
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
            finish();
            return;

        }

        mTextView.setText("Ready to scan");


    }

    public static void resetLabel(){
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(800);
                } catch (InterruptedException e) {
                }
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        scannerActivity.mTextView.setText("Ready to scan");

                    }
                });
            }
        };
        thread.start();
    }

    public static void sendUid(final String uid) {

        final RequestQueue MyRequestQueue = Volley.newRequestQueue(jort);

        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("error no login")) {
                    Log.i(TAG, "not logged in");
                    Intent myIntent = new Intent(scannerActivity.jort, loginActivity.class);
                    scannerActivity.jort.startActivity(myIntent);
                }
                Log.i(TAG,response);
                if(response.contains("scanned")){

                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            scannerActivity.mTextView.setText("scanned");
                            resetLabel();
                        }
                    });
                }
            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
                Log.i(TAG, "volley error in send uid");
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("sessionid", sessionid.replaceAll("\\s+", "")); //Add the data you'd like to send to the server.
                MyData.put("uid",uid);
                MyData.put("subject",subject);
                return MyData;
            }
        };

        MyRequestQueue.add(MyStringRequest);
    }


}
@RequiresApi(api = Build.VERSION_CODES.KITKAT)
class cardreaderp implements NfcAdapter.ReaderCallback {

    @Override
    public void onTagDiscovered(Tag tag) {
        String uid = new String(tag.getId());



        StringBuilder sb = new StringBuilder();
        byte[] id = tag.getId();
        sb.append(toDec(id));
        final String uidDec = sb.toString();
        Log.d(TAG,uidDec);
        scannerActivity.sendUid(uidDec);
    }
    private long toDec(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = 0; i < bytes.length; ++i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }

}
