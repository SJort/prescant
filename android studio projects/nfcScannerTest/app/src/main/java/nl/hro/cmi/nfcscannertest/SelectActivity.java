package nl.hro.cmi.nfcscannertest;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SelectActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private ListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public static final String TAG = "subjectSelectScreen";

    public final static String url = "https://www.warmvogel.nl/php/tes.php";
    final String sessionid = MainActivity.sessionId;
    public ArrayList<Item> responseResult = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //default setup shit
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);

        setTitle("PreScant");
        //create ArrayList with Item's to fill the RecyclerView with
        final ArrayList<Item> itemList = new ArrayList<>();
        itemList.add(new Item(R.drawable.ic_chevron_right, "Line 1", "Line 2"));
        itemList.add(new Item(R.drawable.ic_chevron_right, "Line 3", "Line 4"));
        itemList.add(new Item(R.drawable.ic_chevron_right, "Line 5", "Line 6"));


        //fill the shit, the onClickListener is handled within this method, see comment there
        getSubjects();

    }


    public ArrayList<Item> parseJson(String jason) {
        Statics.log(jason);
        ArrayList<Item> result = new ArrayList<>();
        try {
            JsonElement jsonElement = new JsonParser().parse(jason);
            if (jsonElement.isJsonArray()) {
                JsonArray jsonArray = jsonElement.getAsJsonArray();
                for (JsonElement element : jsonArray) {
                    if (element.isJsonObject()) {
                        //{"Message_id":11,"timestamp":"2019-01-31 12:33:32","Picture_location":"\/..\/foler\/pic11.png","User_id":5,"Ticket_id":3,"Message_String":"ahhhh"}
                        String userId = element.getAsJsonObject().get("lessonID").getAsString();
                        result.add(new Item(R.drawable.ic_chevron_right, userId, ""));
                    } else {
                        Statics.log("Element in array is not a json object.");
                    }
                }
            } else {
                Statics.log("Retrieved data is not a json array.");
            }
        } catch (Exception e) {
            Statics.log("Exception parsing JSON: " + e.getMessage());
            e.printStackTrace();
        }

        return result;
    }

    public void fillRecyclerView(final ArrayList<Item> items){
        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new ListAdapter(items);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new ListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Item item = items.get(position);

                //WHAT TO DO WITH THE CLICKED ITEM
                //Toast.makeText(getApplicationContext(), item.getText1() + " clicked!", Toast.LENGTH_SHORT).show();
                MainActivity.subject = item.getText1();
                Intent myIntent = new Intent(SelectActivity.this, scannerActivity.class);
                SelectActivity.this.startActivity(myIntent);
            }
        });
    }

    public void getSubjects() {

        final RequestQueue MyRequestQueue = Volley.newRequestQueue(this);

        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.i(TAG, "response: " + response);
                if (response.contains("error no login")) {
                    Intent myIntent = new Intent(SelectActivity.this, loginActivity.class);
                    SelectActivity.this.startActivity(myIntent);
                }else{
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            //textView.setText(response);
                            responseResult = parseJson(response);
                            fillRecyclerView(responseResult);
                        }
                    });
                }
            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
                Log.i(TAG, "volley error in send uid");
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("sessionid", sessionid.replaceAll("\\s+", "")); //Add the data you'd like to send to the server.
                MyData.put("lessons","some data");
                return MyData;
            }
        };

        MyRequestQueue.add(MyStringRequest);
    }
}

