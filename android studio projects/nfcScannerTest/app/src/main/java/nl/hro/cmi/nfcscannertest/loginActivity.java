package nl.hro.cmi.nfcscannertest;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class loginActivity extends AppCompatActivity {
    public static final String TAG = "loginScreen";
    String sessionid = "";
    Button btn;
    EditText passw;
    EditText usern;
    public static TextView textView;
    public final String url = "https://www.warmvogel.nl/php/tes.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        textView = (TextView) findViewById(R.id.text);
        setTitle("PreScant");

        btn = (Button) findViewById(R.id.btn);
        usern = (EditText) findViewById(R.id.usern);
        usern.setText("oelew");
        passw = (EditText) findViewById(R.id.passw);
        passw.setText("oelew");
        textView.setText("Welcome, please input username & password");

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
                //sendUid(textView, url, sessionid);
            }
        });
    }

    void login() {
        textView.setText("attempting to log in...");

        final RequestQueue MyRequestQueue = Volley.newRequestQueue(this);

        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                StringBuilder s = new StringBuilder();
                if (response.trim().equals("error no password")) {
                    s.append("please input a password");
                } else if (response.trim().equals("error no username")) {
                    s.append("please input a username");
                } else if (response.trim().equals("error invalid login")) {
                    s.append("invalid login, please try again");
                } else if (response.trim().equals("error not a teacher")) {
                    s.append("please login with a teacher account");
                } else {
                    sessionid = response;
                    MainActivity.sessionId = sessionid;
                    //Log.i(TAG, "|"+response.trim()+"|");
                    s.append("redirecting...");
                    Intent myIntent = new Intent(loginActivity.this, SelectActivity.class);
                    loginActivity.this.startActivity(myIntent);
                }
                final String string = s.toString();
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(string);
                    }
                });
            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionid = "error";
                textView.setText("network error");
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> MyData = new HashMap<String, String>();
                MyData.put("password", passw.getText().toString());
                MyData.put("username", usern.getText().toString());
                MyData.put("request", "hoi");
                return MyData;
            }
        };

        MyRequestQueue.add(MyStringRequest);
    }



}
