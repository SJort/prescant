<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--Fancy-->
    <link rel="shortcut icon" type="image/png" href="/images/favicon.png"/>

    <title>PreScant - about</title>


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="/css/bootstrap/bootstrap-grid.css">
    <link rel="stylesheet" href="/css/bootstrap/bootstrap-reboot.css">

    <!--Third party libraries-->
    <script src="/js/jquery-3.3.1.js"></script>
    <script src="/js/bootstrap/bootstrap.bundle.js"></script>

    <!--My scripts-->
    <script src="/js/base-devel.js"></script>
    <script src="/js/navigation-bar.js"></script>
    <script src="/test/testscript.js"></script>
    <script src="/js/percentage-presence-table.js"></script>


</head>
<body>
<nav id="navigationBar">
</nav>

<div class="container">

    <div class="progress">
        <div class="progress-bar bg-warning" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">3/11</div>
    </div>

    <div id="selectorHolder" class="d-flex flex-sm-row flex-column justify-content-center">
        <div class="p-2">
            <button onclick="stuurKanker()" type="button" class="btn btn-primary">Stuur kanker</button>
        </div>

        <div class="p-2">
            <button onclick="ontvangKanker()" type="button" class="btn btn-primary">Ontvang kanker</button>
        </div>

        <div class="p-2">
            <button onclick="stuurEnOntvangKanker()" type="button" class="btn btn-primary">Stuur, en ontvang kanker!</button>
        </div>
    </div>

    <h3 class="card-title">Log</h3>
    <div class="card">
        <div class="card-body" id="logBar"></div>
    </div>
</div>


</body>
</html>


