$(document).ready(function () {
    log("kanker");
});

let url = "/test/testserver.php?request=";
let kanker = "eenkanker";

function stuurKanker(){
    $.ajax({
        type: 'GET',
        url: url + "set&kanker=" + kanker,
        success: function () {
            log("kanker verstuurd :)");
        },
        error: function () {
            log("kanker mislukt te sturen :(");
        }
    });
}

function stuurEnOntvangKanker(){
    $.ajax({
        type: 'GET',
        url: url + "set&kanker=" + kanker + "&getKanker",
        success: function (khonker) {
            log("kanker verstuurd :), we ontvingen " + khonker + " terug!");
        },
        error: function () {
            log("kanker mislukt te sturen :(");
        }
    });

}

function ontvangKanker(){
    $.ajax({
        type: 'GET',
        url: url + "getKanker",
        success: function (khonker) {
            log("de volgende kanker is ontvangen: " + khonker);
        },
        error: function () {
            errorLog("we konden geen kanker ontvangen :(");
        }
    });
}

